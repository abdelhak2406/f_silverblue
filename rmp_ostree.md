# Upgrading fedora from release to another

you can just follow [this tutorial](https://docs.fedoraproject.org/en-US/fedora-silverblue/updates-upgrades-rollbacks/) but i had a problem since i added the rpm-fusin packages so what i did is this

```bash
rpm-ostree rebase fedora:fedora/35/x86_64/silverblue \ --uninstall rpmfusion-free-release-34-1.noarch \ --uninstall rpmfusion-nonfree-release-34-1.noarch \ --install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-35.noarch.rpm \ --install https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-35.noarch.rpm
```

- thanks to this guy in [here](https://discussion.fedoraproject.org/t/cant-rebase-to-silverblue-33/29737)
- you just need to replace your current fedora version with the `34` and your futur one with `35` and that's it


