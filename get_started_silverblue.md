# rpm-ostree installation

**lISTE d'utilisatire a installé si j'utilise fedora silverblue**
- vim 
- htop
- neofetch
- cmatrix
- thunar
- emacs(dispo sur flatpack aussi normalement):
- lazygit( maybe on toolbox mais still worthy)

- tilix (very nice terminal)
- Anaconda (for ai stuff!)
- Zoxyde : a better cd command
- fzf very useful for all kinfs of stuf (like combined with zodyde using the `zi` command)

## network things

- kmod-wl (NETWORK driver)
i used [this](https://www.cyberciti.biz/faq/fedora-linux-install-broadcom-wl-sta-wireless-driver-for-bcm43228/) as a reference
	it didn't work on the main system, but worked on a toolbox.
now i'm gonna try this one 

https://discussion.fedoraproject.org/t/wifi-broadcom-drivers/306/9
- **fixed**: by adding external repo to rpm-ostree

```
sudo rpm-ostree install https://mirrors.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm https://mirrors.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm 
```
refs:
	- https://discussion.fedoraproject.org/t/add-external-repos/18951
	- https://rpmfusion.org/Configuration
and then install 

```
sudo rpm-ostree install kmod-wl 
```

## flatpack (try to find flatpack at least otherwise we'll see)

try to use flatpack for gui apps, i'm still wondering how to deal with non flatpack things, if i'll use a toolbox or something else!

### found

- vlc 
- mpv
- telegram
- discord (com.discordapp.Discord )
- vscode (apparently it's not the official package!)
- flameshot
- vscodium
- libreoffice
- spotify

- SOLANUM pomodoro application
- Extensions to check if anything is open(discord and telegram)

### not found

brave
vivaldi
neovim
thunar-file-manager

> for thoose just use a toolbox and create a .desktop file, gonna try to talk about it later

## TODO

- [ ] install music and movies codecs https://docs.fedoraproject.org/en-US/quick-docs/assembly_installing-plugins-for-playing-movies-and-music/
	- [x] you need to enable the rpm fusion repositories https://docs.fedoraproject.org/en-US/quick-docs/setup_rpmfusion/
- [x] ajouter shortcut pour terminal.
- [ ] Bluettooth off by default on boot(startup).
- [ ] bouton droit touchpad don't work 
- [X] separer les fenetre d'une seul application (terminal par exemple)
- [X] separer les fentre d'un workspace a un autre, quand je fait shift+tab sa affiche toutes les fentre de tout les workspace, et je ne veut pas sa!
- rendre plus rapide les animation quand je met une fenetre a gauche ou a droite
- [ ] inverser Maj et echap

## Installed inside toobox

check the [file here](/toolbox_tools_installed.md)

## WHAT IS A TOOLBOX?**

- i created one called first_toolbox
- i installed the kmod-wl inside of it, the commands dont work :)
- i installed gnome-tweaks inside of it too which makes me obligated to enter the toolbox `first_toolbox` to launch gnome-tweaks from the CLI
- i instaled vsCode inside the tooblox (following the official fedora installation on the website)
- i installed brave on toolbox.(to launch it use brave-browser)
- i installed java 11 folowing the official fedora docs (the dev version)
- i installed scence builder and copied the .desktop ffrom `/opt/scenebuilder/lib` to the `~/.local/share/applications/` using the command 
```bash
cp scenebuilder-SceneBuilder.desktop ~/.local/share/applications/
```
- i created a second toolbox, just for virtualbox

## THINGS TO READ AND WATCH 

- add external repos witout having dnf https://discussion.fedoraproject.org/t/add-external-repos/18951
- https://docs.fedoraproject.org/en-US/fedora-silverblue/technical-information/
- https://www.youtube.com/watch?v=NLy0Eg-tblM
- https://www.linuxtricks.fr/wiki/fedora-silverblue-memo-des-commandes-rpm-ostree-et-ostree
- https://fedoramagazine.org/what-is-silverblue/

## Thoughts

I think i'm gonna try like zoid said to have a clean base system, so maybe i'll use a toolbox for installing everything which is not flapack but have a gui?
and also some kindof usell cli apps that 

## Update

use the command
```rpm-ostree update```

it's equivalent to apt update & apt upgrade






