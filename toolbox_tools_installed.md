This file aims to contain all the things i installed in different toolboxes.

# first_toolbox

- superproductivity
- vscode
- brave
- gnome tweaks
- lazygit
- java
- scenceBuilder 
- melde http://meldmerge.org/ (think of updating it regularly)
	i used the [flatpak obviously](https://flathub.org/apps/details/org.gnome.meld)

# How to add a desktop icon (launcher) for a toolbox app

1. go to the `~/.local/share/applications`
2. follow this tutorial https://linuxconfig.org/how-to-create-custom-desktop-files-for-launchers-on-linux
3. in the exec write `toolbox run -c container_name application-name` (https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/)
4. save and enjoy
5. you might need to log in and out


# How to upgrade a toolbox

the goal is to update my fedora 34 toolbox to a fedora 35. here's the commands.

```bash
sudo dnf upgrade --refresh
sudo dnf install dnf-plugin-system-upgrade
sudo dnf system-upgrade download --releasever=35
sudo dnf system-upgrade reboot
sudo dnf system-upgrade upgrade -y
```
## Note 

- the `-y` is just to accept the things automatically during the installation.
- even if you have errors when doing the `sudo dnf system-upgrade reboot` command it's okay, i did and it's normal since toolboxes are independent of systemd things.


# Ressources 

- https://docs.fedoraproject.org/en-US/fedora-silverblue/getting-started/
- https://docs.fedoraproject.org/en-US/quick-docs/upgrading/
- https://docs.fedoraproject.org/en-US/quick-docs/dnf-system-upgrade/
